#Requires -PSEdition Core
Set-Location -Path $PSScriptRoot -Verbose
<#

Title: SFTP to Local Share - Get
Author: Michael J. Acosta
Date: 6/9/2021

Requirements:
    -Powershell Version:
        Tested with Powershell Core 7.1.3 on Windows 10/Server 2016.
    - Powershell Modules:
        POSH-SSH,Microsoft.PowerShell.SecretStore,Microsoft.PowerShell.SecretManagement
    
    -Microsoft SecretName and Password:
        Please see https://devblogs.microsoft.com/powershell/secretmanagement-and-secretstore-are-generally-available/#:~:text=The%20SecretManagement%20module%20helps%20users%20manage%20secrets%20by,unregistered%20for%20use%20in%20accessing%20and%20retrieving%20secrets. on how to manage Secrets inside the a Microsoft Vault.

        -PassFile(srv_credential.xml) and (KeyFile)srv_aes.key Generated for accessing the Microsoft Vault and Secret(if using password instead of ssh key, for ssh key please see the notes for using the -Keyfile paramter below).

INFO:
    This script will not work with a KeyFile, a variable with the path to the key and the -KeyFile parameter needs to be added to the sftpConnectSession function.
    If a key file is used, make sure to comment out the getSecret function and hardcode the username.
    Known hosts are stored at %USERPROFILE%\.poshssh\hosts.json
    If you run into issues with the thumbprint, make sure to use the Get-SSHTrustedHost cmdlet.
    You can schedule this script with Windows Task Scheduler or a more advance CI/CD using C:\Program Files\PowerShell\7\pwsh.exe -file sftp_to_share.ps1

NOTE:
    For Egnyte, make sure to use the username$domain convention as noted on their article: https://helpdesk.egnyte.com/hc/en-us/articles/201637844-Commandline-FTP

    If you are moving files, leave script as is.
    If you are copying files, comment out the removeItems function in the #Execute section.


#>
#-------------------------
#-------------------------
#   START OF VARIABLES  --
#-------------------------
#-------------------------

#SFTP Server Information
$SFTPServer = "ftp-stevemadden.egnyte.com"
$SFTPPort = "22"
$SFTPPath = "/Shared/Dolce Vita/Images/SFTP_Test/"

#Local/Network UNC Path Information
$LocalPath = ".\Received_From_SFTP"

#Microsoft Secre Store Management
#PassFile and Key for Unlock-SecretStore
$PassFile = ".\srv_credential.xml"
$KeyFile = Get-Content ".\srv_aes.key"
#Vault and Secret
$VaultName = "Production"
$SecretName = "sftp_egnyte`$stevemadden"

#Logging
$LogFile = '.\sftp_egnyte.log'

#Logging | Email
$MailHast = @{
    From       = "sftp_egnyte@stevemadden.com"
    Subject    = "Error Processing $SFTPServer on " + (Get-CimInstance -ClassName Win32_ComputerSystem).Name
    To         = @("michaelacosta@stevemadden.com")
    SMTPServer = "mailrelay.smcorp.local"
}

#-------------------------
#-------------------------
#   END OF VARIABLES    --
#-------------------------
#-------------------------
function Add-Log{
    [CmdletBinding()]
    param 
    (
        [Parameter(Mandatory = $true)]
        [string]$LogString,
        [Parameter(Mandatory = $true)]
        [string]$LogFile
    )
    $date = Get-Date -UFormat "%m/%d/%Y %H:%M:%S"
    $logstring | ForEach-Object { Add-Content $LogFile -Value "$date - $_" }
}

function checkSystem{
    $HostInformation = Get-ComputerInfo | Select-Object WindowsProductName,OsInstallDate,OsArchitecture,CsUserName | Out-String
    $NetworkInformation = Get-NetIPConfiguration | Select-Object -Property IPv4Address | Out-String
    Add-Log -LogString "Checking System Information..." -LogFile $LogFile
    Add-Log -LogString "$HostInformation" -LogFile $LogFile
    Add-Log -LogString "$NetworkInformation" -LogFile $LogFile
}


function checkModules{
    Add-Log -LogString "Importing Powershell SFTP and Vault Modules..." -LogFile $LogFile
    Import-Module -Name POSH-SSH,Microsoft.PowerShell.SecretStore,Microsoft.PowerShell.SecretManagement -Verbose
    if($?){
        Add-Log -LogString "Modules POSH-SSH,Microsoft.PowerShell.SecretStore,Microsoft.PowerShell.SecretManagement are installed." -LogFile $logFile
        }
        else {
            $body = "ERROR --  Unable to load Powershell Modules. Check that POSH-SSH,Microsoft.PowerShell.SecretStore,Microsoft.PowerShell.SecretManagement are installed before running this script. "
            Send-MailMessage @MailHast -Body $body
            Add-Log -LogString $body -LogFile $LogFile
            exit 1
            }

}


function getSecret{
    Add-Log -LogString "Unlocking Microsoft Store Management Vault..." -LogFile $LogFile
    $UnlockVault = Import-CliXml $PassFile | ConvertTo-SecureString -Key $KeyFile

    if(-not $UnlockVault){
        ## Send email with error and return
        $body = "ERROR -- Unable to open local Microsoft Secret Store. Missing or Incorrect Password File or Key File."
        Send-MailMessage @MailHast -Body $body
        Add-Log -LogString $body -LogFile $logFile
        return
    }
    else{
        Unlock-SecretStore -Password $UnlockVault -Verbose
            if(!$?){
                $body = "ERROR -- Unable to open the Microsoft Secret Store. Troubleshoot Vault and Secret."
                Send-MailMessage @MailHast -Body $body
                Add-Log -LogString $body -LogFile $logFile
                return
            }
    }

   Add-Log -LogString "Unlocking Vault succesfull. Secret ready for SFTP connection..." -LogFile $LogFile
}


function sftpConnectSession{

    $SFTPSecret = Get-Secret -Vault $VaultName -Name $SecretName
    $SFTPCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $SecretName,$SFTPSecret
    
    $Fingerprint = Get-SSHTrustedHost | Where-Object {$_.HostName -eq $SFTPServer} | Out-String
    Add-Log -LogString "Collecting Fingerprint for logging: $Fingerprint" -LogFile $logFile

    Add-Log -LogString "Connecting to $SFTPServer...." -LogFile $logFile

    $Global:SFTPSession  = New-SFTPSession -ComputerName $SFTPServer -Port $SFTPPort -AcceptKey -Credential $SFTPCredential -Verbose

    if (-not $SFTPSession) {
        $body = "ERROR -- Cannot connect to $SFTPServer, make sure that username and password stored on the Microsoft Secret Store are correct. Check logs for additonal information including thumbprint used."
        Send-MailMessage @MailHast -Body $body
        Add-Log -LogString $body -LogFile $LogFile
        Return
    }
    else {
        $sftpstatus = ($SFTPSession | Out-String)
        Add-Log -LogString $sftpstatus -LogFile $LogFile
    }

}


function sftpPathTest{
    $TestSFTPPath = Test-SFTPPath -SessionId $SFTPSession.SessionId -Path $SFTPPath
    if($TestSFTPPath){
        Add-Log -LogString "Remote Location $SFTPPath exists, continuing..." -LogFile $logFile
        }else{
        $body = "ERROR -- $SFTPServer Remote location $SFTPPath does not exists, Aborting!"
        Send-MailMessage @MailHast -Body $body
        Add-Log -LogString $body -LogFile $logFile
        exit 1
        }
}

function localPathTest{
    $TestLocalPath = Test-Path -Path $LocalPath
    if($TestLocalPath){
        Add-Log -LogString "Local Location $LocalPath exists, continuing..." -LogFile $logFile
        }else{
        $body = "ERROR -- $SFTPServer local location $LocalPath does not exists, Aborting!"
        Send-MailMessage @MailHast -Body $body
        Add-Log -LogString $body -LogFile $logFile
        exit 1
        }
}


function checkItemsAvailability{
    $SFTPCChilds = Get-SFTPChildItem -SessionId $SFTPSession.SessionId -Path $SFTPPath | Where-Object {$_.Name -notlike "*." -and $_.Name -notlike "*.."}
    $SFTPItems = $SFTPCChilds.count
    if($SFTPItems -le 0){
        Add-Log -LogString "INFO -- There are no new files to download at $SFTPServer $SFTPPath." -LogFile $logFile
        return
    }else{
        Add-Log -LogString "Found a total of $SFTPItems Items." -LogFile $logFile
    }
}

function logCurrentItems{
    Add-Log -LogString "Listing current items before doing any actions for logging purposes." -LogFile $logFile
    $SFTPItemsLog = Get-SFTPChildItem -SessionId 0 -Path $SFTPPath -Recursive | Out-String
    Add-Log -LogString "$SFTPItemsLog" -LogFile $logFile
}


function copyItems{

    Get-SFTPChildItem -SessionId $SFTPSession.SessionId -Path $SFTPPath | Where-Object {$_.Name -notlike "*." -and $_.Name -notlike "*.."} `
    |  ForEach-Object {
       $MeasureTransfer = Measure-Command {Get-SFTPItem -SessionId $SFTPSession.SessionId -Path $_.FullName -Destination $LocalPath -Verbose -Force} | Out-String
       Add-Log -LogString "INFO -- Copying $_.FullName $MeasureTransfer" -LogFile $logFile
        }
        
}

function removeItems{

    Get-SFTPChildItem -SessionId $SFTPSession.SessionId -Path $SFTPPath | Where-Object {$_.Name -notlike "*." -and $_.Name -notlike "*.."} `
    |  ForEach-Object {
       $MeasureTransfer = Measure-Command {Remove-SFTPItem -SessionId $SFTPSession.SessionId -Path $_.FullName -Verbose -Force} | Out-String
       Add-Log -LogString "INFO -- Removing $_.FullName $MeasureTransfer" -LogFile $logFile
        }
        
}

function sftpDisconnectSession{
    if ($SFTPSession = Get-SFTPSession -SessionId $SFTPSession.SessionId) {
        $SFTPSession.Disconnect()
    }
    $null = Remove-SftpSession -SftpSession $SFTPSession -Verbose

    Add-Log -LogString "Disconnecting from $SFTPServer... Good Bye!" -LogFile $logFile

    Get-SFTPSession

    
}


#Execute
Add-Log -LogString "-------------------------------------------------------------------------------" -LogFile $LogFile
Add-Log -LogString "-----------------                Start Run                     ----------------" -LogFile $LogFile
Add-Log -LogString "-------------------------------------------------------------------------------" -LogFile $LogFile

checkSystem
checkModules
getSecret
sftpConnectSession
sftpPathTest
localPathTest
checkItemsAvailability
logCurrentItems
copyItems
removeItems
sftpDisconnectSession

Add-Log -LogString "-------------------------------------------------------------------------------" -LogFile $logfile
Add-Log -LogString "-----------------                 End Run                      ----------------" -LogFile $logfile
Add-Log -LogString "-------------------------------------------------------------------------------" -LogFile $logfile

exit 0
